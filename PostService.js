import Post from "./Post.js";
import fileService from "./fileService.js";
class PostService {
  async create(post, picture) {
    const fileName = fileService.saveFile(picture);
    const createdPosts = await Post.create({ ...post, photo: fileName });
    return createdPosts;
  }

  async getAll() {
    const allPosts = await Post.find();
    return allPosts;
  }

  async getOne(id) {
    if (!id) {
      throw new Error("Id Not Find");
    }
    const postById = await Post.findById(id);
    return postById;
  }

  async update(post) {
    if (!post._id) {
      throw new Error("Id Not Found");
    }
    const updatedPost = await Post.findByIdAndUpdate(post._id, post, {
      new: true,
    });
    return updatedPost;
  }

  async delete(id) {
    if (!id) {
      throw new Error("Id Not Found");
    }
    const deletedPost = await Post.findByIdAndDelete(id);
    return deletedPost;
  }
}
export default new PostService();
