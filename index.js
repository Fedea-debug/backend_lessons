import express from "express";
import mongoose from "mongoose";
import route from "./router.js";
import fileUpload from "express-fileupload";

const PORT = 5000;

const URL_DB = ` mongodb+srv://Teo:527447252003Td!@cluster0.2qjwx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
const app = express();

app.use(express.json());
app.use(fileUpload({}));
app.use("/api", route);

async function startApp() {
  try {
    await mongoose.connect(URL_DB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => console.log("SERVER IS WORKING ON PORT" + PORT));
  } catch (e) {
    console.log(e);
  }
}

startApp();
